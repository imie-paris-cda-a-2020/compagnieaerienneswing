CREATE DATABASE  IF NOT EXISTS `airbabouche` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `airbabouche`;
-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: airbabouche
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aeroport`
--

DROP TABLE IF EXISTS `aeroport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aeroport` (
  `idaeroport` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `ville` varchar(70) NOT NULL,
  `idPays` int(11) NOT NULL,
  PRIMARY KEY (`idaeroport`),
  KEY `fk_aeroport_pays_idx` (`idPays`),
  CONSTRAINT `fk_aeroport_pays` FOREIGN KEY (`idPays`) REFERENCES `pays` (`idPays`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aeroport`
--

LOCK TABLES `aeroport` WRITE;
/*!40000 ALTER TABLE `aeroport` DISABLE KEYS */;
INSERT INTO `aeroport` VALUES (3,'Charles de Gaulle','Paris',1),(5,'Aéroport de Nice','Nice',1),(6,'JFK International Airport','New York',7),(7,'Montréal Airport','Montréal',8);
/*!40000 ALTER TABLE `aeroport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `client` (
  `email` varchar(200) NOT NULL,
  `password` varchar(80) NOT NULL,
  `personne_idpersonne` int(11) NOT NULL,
  PRIMARY KEY (`personne_idpersonne`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  CONSTRAINT `fk_client_personne1` FOREIGN KEY (`personne_idpersonne`) REFERENCES `personne` (`idpersonne`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES ('test3','12345',1),('test2','test',12),('testtesttesttesttest','test',17),('azfzafazazgzagb','test',21),('kikigmail.com','1234',22),('kiki@gmail.com','test',23),('adressemail','1234',24),('adressemailazdazf','1234',25),('test@gmail.com','test',26),('elie.yaffa@gmail.com','1234',27),('vincent.cassel@gmail.com','1234',28),('vincent.cassel2@gmail.com','1234',29),('testmail@gmail.com','a',31),('testmail2@gmail.com','a',33),('testmail2zada@gmail.com','12345',39);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compagnie`
--

DROP TABLE IF EXISTS `compagnie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `compagnie` (
  `idcompagnie` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`idcompagnie`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compagnie`
--

LOCK TABLES `compagnie` WRITE;
/*!40000 ALTER TABLE `compagnie` DISABLE KEYS */;
INSERT INTO `compagnie` VALUES (1,'AirFrance'),(3,'AmericanAirlines'),(4,'AirCanada'),(5,'AirCaraibes'),(6,'EasyJet'),(7,'AirAlgerie');
/*!40000 ALTER TABLE `compagnie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escale`
--

DROP TABLE IF EXISTS `escale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `escale` (
  `vol_idvol` int(11) NOT NULL,
  `aeroport_idaeroport` int(11) NOT NULL,
  `date_depart_escale` datetime NOT NULL,
  `date_arrive_escale` datetime NOT NULL,
  PRIMARY KEY (`vol_idvol`,`aeroport_idaeroport`),
  KEY `fk_escale_aeroport_idx` (`aeroport_idaeroport`),
  CONSTRAINT `fk_escale_aeroport` FOREIGN KEY (`aeroport_idaeroport`) REFERENCES `aeroport` (`idaeroport`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_escale_vol1` FOREIGN KEY (`vol_idvol`) REFERENCES `vol` (`idvol`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escale`
--

LOCK TABLES `escale` WRITE;
/*!40000 ALTER TABLE `escale` DISABLE KEYS */;
INSERT INTO `escale` VALUES (10,6,'2021-01-10 17:45:00','2021-01-10 16:10:00'),(11,6,'2021-01-10 17:45:00','2021-01-10 16:10:00'),(12,5,'2021-01-10 17:45:00','2021-01-10 16:10:00'),(14,5,'2021-01-10 14:45:00','2021-01-10 13:15:00');
/*!40000 ALTER TABLE `escale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passager`
--

DROP TABLE IF EXISTS `passager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `passager` (
  `idpassager` int(11) NOT NULL AUTO_INCREMENT,
  `idpersonne` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpassager`),
  KEY `FK_passager_personne` (`idpersonne`),
  CONSTRAINT `FK_passager_personne` FOREIGN KEY (`idpersonne`) REFERENCES `personne` (`idpersonne`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passager`
--

LOCK TABLES `passager` WRITE;
/*!40000 ALTER TABLE `passager` DISABLE KEYS */;
INSERT INTO `passager` VALUES (1,1),(2,1),(3,38),(4,39),(5,39),(6,39);
/*!40000 ALTER TABLE `passager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pays`
--

DROP TABLE IF EXISTS `pays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pays` (
  `idPays` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`idPays`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pays`
--

LOCK TABLES `pays` WRITE;
/*!40000 ALTER TABLE `pays` DISABLE KEYS */;
INSERT INTO `pays` VALUES (1,'France'),(2,'Belgique'),(3,'Irlande'),(4,'Afghanistan'),(5,'Maroc'),(7,'Etats-Unis'),(8,'Canada');
/*!40000 ALTER TABLE `pays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personne`
--

DROP TABLE IF EXISTS `personne`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personne` (
  `idpersonne` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(70) NOT NULL,
  `prenom` varchar(70) NOT NULL,
  `dateNaiss` date NOT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `idPays` int(11) NOT NULL,
  PRIMARY KEY (`idpersonne`),
  KEY `fk_personne_pays_idx` (`idPays`),
  CONSTRAINT `fk_personne_pays` FOREIGN KEY (`idPays`) REFERENCES `pays` (`idPays`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personne`
--

LOCK TABLES `personne` WRITE;
/*!40000 ALTER TABLE `personne` DISABLE KEYS */;
INSERT INTO `personne` VALUES (1,'test','test','2021-01-08','test','test','test',1),(2,'test','test','2021-01-08','test','test','test',1),(4,'test','test','2021-01-08','test','test','test',1),(5,'test','test','2021-01-08','test','test','test',2),(6,'test','test','2021-01-08','test','test','test',1),(7,'test','test','2021-01-08','test','test','test',2),(8,'test','test','2021-01-08','test','test','test',1),(9,'test','test','2021-08-01','test','test','test',1),(10,'Mbappé','Kylian','2000-01-01','15 rue des champs','Paris','75016',1),(12,'test','test','2021-01-08','aa','aa','aa',1),(13,'test','test','2021-01-08','test','test','test',1),(14,'test','test','2021-01-08','test','test','test',1),(15,'test','test','2021-01-08','test','test','test',1),(16,'test','test','2021-01-08','test','test','test',1),(17,'test','test','2021-01-08','test','test','test',1),(18,'test','test','2021-08-01','test','test','test',1),(19,'test','test','2021-01-08','test','test','test',1),(20,'test','test','2021-01-08','test','test','test',1),(21,'test','test','2002-01-01','test','test','test',1),(22,'Mbappé','Kylian','2021-01-08','15 rue des champs','Paris','75016',1),(23,'test','test','2021-01-08','test','test','test',1),(24,'test','test','2021-01-08','test','test','test',1),(25,'test','test','2021-01-08','test','test','test',2),(26,'test','test','2021-01-08','test','test','test',1),(27,'Yaffa','Elie','1978-09-28','475 Miami Beach avenue','Miami','94600',1),(28,'Cassel','Vincent','1978-09-02','15 rue longchamps','Paris','7500',4),(29,'Cassel','Vincent','1978-09-02','15 rue longchamps','Paris','75000',4),(31,'a','a','2020-01-01','a','a','a',4),(33,'a','a','2020-01-01','a','a','a',4),(34,'Winchester','Dean','1978-08-02','20 Hellkitchen avenue','New York','49000',7),(35,'a','a','2000-01-01','a','a','a',4),(36,'Winchester','Sam','1978-09-22','259 splash avenue','Colorado','84123',7),(37,'Test','Test','2000-01-01','rue du test','Paris','50000',8),(38,'Test','Test','2000-01-01','Rue du test','Paris','75000',1),(39,'Test','Test','2021-01-01','test','test','test',4);
/*!40000 ALTER TABLE `personne` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservation` (
  `idvol` int(11) NOT NULL,
  `client_idpersonne` int(11) NOT NULL,
  `confirmation` enum('Confirm','Pending','Cancel','Refuse') NOT NULL,
  `personne_idpersonne` int(11) NOT NULL,
  KEY `FK_reservation_vol` (`idvol`),
  KEY `fk_reservation_client1_idx` (`client_idpersonne`),
  KEY `fk_reservation_personne1_idx` (`personne_idpersonne`),
  CONSTRAINT `FK_reservation_vol` FOREIGN KEY (`idvol`) REFERENCES `vol` (`idvol`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_reservation_client1` FOREIGN KEY (`client_idpersonne`) REFERENCES `client` (`personne_idpersonne`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_reservation_personne1` FOREIGN KEY (`personne_idpersonne`) REFERENCES `personne` (`idpersonne`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` VALUES (5,1,'Confirm',1),(5,1,'Pending',38),(10,39,'Pending',39),(9,39,'Pending',39),(5,39,'Confirm',39);
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vol`
--

DROP TABLE IF EXISTS `vol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vol` (
  `idvol` int(11) NOT NULL AUTO_INCREMENT,
  `place` smallint(6) NOT NULL DEFAULT '300',
  `intitule` varchar(50) DEFAULT NULL,
  `aeroport_depart` int(11) NOT NULL,
  `aeroport_arrive` int(11) NOT NULL,
  `date_depart` datetime NOT NULL,
  `date_arrive` datetime NOT NULL,
  `idcompagnie` int(11) NOT NULL,
  PRIMARY KEY (`idvol`),
  KEY `fk_vol_compagnie_idx` (`idcompagnie`),
  KEY `fk_vol_aeroport_depart_idx` (`aeroport_depart`),
  KEY `fk_vol_aeroport_arrivee_idx` (`aeroport_arrive`),
  CONSTRAINT `fk_vol_aeroport_arrivee` FOREIGN KEY (`aeroport_arrive`) REFERENCES `aeroport` (`idaeroport`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_vol_aeroport_depart` FOREIGN KEY (`aeroport_depart`) REFERENCES `aeroport` (`idaeroport`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_vol_compagnie` FOREIGN KEY (`idcompagnie`) REFERENCES `compagnie` (`idcompagnie`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vol`
--

LOCK TABLES `vol` WRITE;
/*!40000 ALTER TABLE `vol` DISABLE KEYS */;
INSERT INTO `vol` VALUES (5,42,'Vol Paris CDG - New York JFK',5,5,'2021-10-10 15:50:00','2021-01-11 00:00:00',1),(6,42,'Vol Montréal - New York',3,6,'2021-01-10 00:00:00','2021-01-11 00:00:00',1),(8,42,'test',5,7,'2021-01-10 01:00:00','2021-01-10 09:00:00',6),(9,50,'Vol Test',3,7,'2021-01-10 10:10:00','2021-01-10 17:50:00',4),(10,50,'Vol Test avec escale',3,7,'2021-01-10 10:10:00','2021-01-10 19:30:00',4),(11,50,'Vol Test avec escale',3,7,'2021-01-10 10:10:00','2021-01-10 19:30:00',4),(12,50,'Vol Test avec escale',3,7,'2021-01-10 10:10:00','2021-01-10 19:30:00',4),(13,42,'Test vol sans escale',5,3,'2021-01-01 10:10:00','2021-01-01 12:45:00',1),(14,42,'Test vol sans escale',7,3,'2021-01-10 10:10:00','2021-01-10 20:45:00',4);
/*!40000 ALTER TABLE `vol` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-10  5:33:37
