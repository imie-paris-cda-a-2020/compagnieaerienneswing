-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : Dim 10 jan. 2021 à 16:50
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `compagnieaerienneswing`
--

-- --------------------------------------------------------

--
-- Structure de la table `aeroport`
--

CREATE TABLE `aeroport` (
  `IDAEROPORT` int(11) NOT NULL,
  `NOMAEROPORT` varchar(50) NOT NULL,
  `VILLEAEROPORT` varchar(50) NOT NULL,
  `PAYSAEROPORT` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `aeroport`
--

INSERT INTO `aeroport` (`IDAEROPORT`, `NOMAEROPORT`, `VILLEAEROPORT`, `PAYSAEROPORT`) VALUES
(1, 'JFK', 'New York', 'USA'),
(2, 'CDG', 'Paris', 'France'),
(3, 'LAX', 'Los Angeles', 'USA'),
(4, 'DME', 'Moscou', 'Russie'),
(5, 'LED', 'Saint Petersbourg', 'Russie'),
(6, 'CLY', 'Calvi', 'France'),
(7, 'ATH', 'Athènes', 'Grèce');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `IDPERSONNE` int(11) NOT NULL,
  `EMAILCLIENT` varchar(200) NOT NULL,
  `PASSWORDCLIENT` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `compagnie`
--

CREATE TABLE `compagnie` (
  `IDCOMPAGNIE` int(11) NOT NULL,
  `NOMCOMPAGNIE` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `compagnie`
--

INSERT INTO `compagnie` (`IDCOMPAGNIE`, `NOMCOMPAGNIE`) VALUES
(1, 'EasyJet'),
(2, 'Air France'),
(3, 'American Airlines'),
(4, 'Corsair'),
(5, 'HOP!'),
(6, 'Wizz Air'),
(7, 'Aeroflot');

-- --------------------------------------------------------

--
-- Structure de la table `escale`
--

CREATE TABLE `escale` (
  `IDVOLS` int(11) NOT NULL,
  `IDAEROPORT` int(11) NOT NULL,
  `DATEDEPARTVOLS` varchar(50) NOT NULL,
  `DATEARRIVEEVOLS` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `passager`
--

CREATE TABLE `passager` (
  `IDPASSAGER` int(11) NOT NULL,
  `IDPERSONNE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `IDPAYS` int(11) NOT NULL,
  `NOMPAYS` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pays`
--

INSERT INTO `pays` (`IDPAYS`, `NOMPAYS`) VALUES
(1, 'Russie'),
(2, 'Corse'),
(3, 'France'),
(4, 'Suisse'),
(5, 'Grèce'),
(6, 'USA'),
(7, 'Congo');

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `IDPERSONNE` int(11) NOT NULL,
  `NOMPERSONNE` varchar(70) NOT NULL,
  `PRENOMPERSONNE` varchar(70) NOT NULL,
  `NAISSANCEPERSONNE` varchar(100) NOT NULL,
  `ADRESSEPERSONNE` varchar(50) NOT NULL,
  `VILLEPERSONNE` varchar(50) NOT NULL,
  `CPPERSONNE` varchar(20) NOT NULL,
  `IDPAYS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `IDVOLS` int(11) NOT NULL,
  `IDPERSONNE` int(11) DEFAULT NULL,
  `NUMCONFIRMATION` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`IDVOLS`, `IDPERSONNE`, `NUMCONFIRMATION`) VALUES
(1, NULL, 123456),
(2, NULL, 345678),
(3, NULL, 234567),
(5, NULL, 456789);

-- --------------------------------------------------------

--
-- Structure de la table `vols`
--

CREATE TABLE `vols` (
  `IDVOLS` int(11) NOT NULL,
  `PLACEVOLS` varchar(10) NOT NULL,
  `INTITULEVOLS` varchar(100) NOT NULL,
  `AEROPORTDEPARTVOLS` varchar(5) NOT NULL,
  `AEROPORTARRIVEVOLS` varchar(5) NOT NULL,
  `DATEDEPARTVOLS` varchar(50) NOT NULL,
  `DATEARRIVEEVOLS` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `vols`
--

INSERT INTO `vols` (`IDVOLS`, `PLACEVOLS`, `INTITULEVOLS`, `AEROPORTDEPARTVOLS`, `AEROPORTARRIVEVOLS`, `DATEDEPARTVOLS`, `DATEARRIVEEVOLS`) VALUES
(1, '2b', '45678', 'JFK', 'CDG', '08/01/2021', '15/01/2021'),
(2, '15c', '12354', 'ATH', 'CDG', '15/01/2021', '20/01/2021'),
(3, '10g', '45720', 'LAX', 'JFK', '12/01/2021', '22/01/2021');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `aeroport`
--
ALTER TABLE `aeroport`
  ADD PRIMARY KEY (`IDAEROPORT`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`IDPERSONNE`);

--
-- Index pour la table `compagnie`
--
ALTER TABLE `compagnie`
  ADD PRIMARY KEY (`IDCOMPAGNIE`);

--
-- Index pour la table `passager`
--
ALTER TABLE `passager`
  ADD PRIMARY KEY (`IDPASSAGER`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`IDPAYS`);

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`IDPERSONNE`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`IDVOLS`);

--
-- Index pour la table `vols`
--
ALTER TABLE `vols`
  ADD PRIMARY KEY (`IDVOLS`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `aeroport`
--
ALTER TABLE `aeroport`
  MODIFY `IDAEROPORT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `IDPERSONNE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `compagnie`
--
ALTER TABLE `compagnie`
  MODIFY `IDCOMPAGNIE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `passager`
--
ALTER TABLE `passager`
  MODIFY `IDPASSAGER` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `IDPAYS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `personne`
--
ALTER TABLE `personne`
  MODIFY `IDPERSONNE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `IDVOLS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `vols`
--
ALTER TABLE `vols`
  MODIFY `IDVOLS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
