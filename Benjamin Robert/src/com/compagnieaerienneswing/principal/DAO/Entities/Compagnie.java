/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal.DAO.Entities;

import com.compagnieaerienneswing.principal.DAO.Connexion;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author FiercePC
 */
public class Compagnie implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idcompagnie;

    private String nom;

    public Compagnie() {
    }

    public Compagnie(Integer idcompagnie) {
        this.idcompagnie = idcompagnie;
    }

    public Compagnie(Integer idcompagnie, String nom) {
        this.idcompagnie = idcompagnie;
        this.nom = nom;
    }

    public Integer getIdcompagnie() {
        return idcompagnie;
    }

    public void setIdcompagnie(Integer idcompagnie) {
        this.idcompagnie = idcompagnie;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcompagnie != null ? idcompagnie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compagnie)) {
            return false;
        }
        Compagnie other = (Compagnie) object;
        if ((this.idcompagnie == null && other.idcompagnie != null) || (this.idcompagnie != null && !this.idcompagnie.equals(other.idcompagnie))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.compagnieaerienneswing.principal.DAO.Entities.Compagnie[ idcompagnie=" + idcompagnie + " ]";
    }
    
          public static void saveCompagnie(Compagnie comp) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Insert into Compagnie (nom) values(?)  ");
        ps.setString(1, comp.getNom());
        ps.execute();
        ps.close();
        System.err.println("Insert OK");
    }

    public static void updateCompagnie(Compagnie comp) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Update compagnie set nom = ? where idcompagnie = ?  ");
        ps.setString(1, comp.getNom());
        ps.setInt(2, comp.getIdcompagnie());
        ps.executeUpdate();
        ps.close();
        System.err.println("Update OK");
    }

    public static void deleteCompagnie(Compagnie comp) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("delete from compagnie where idcompagnie = ? ");
        ps.setInt(1, comp.getIdcompagnie());
        ps.execute();
        ps.close();
        System.err.println("Delete OK");
    }

    public static ArrayList<Compagnie> getAllCompagnie() throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Select * from compagnie order by idcompagnie");
        ResultSet rs = ps.executeQuery();
        ArrayList<Compagnie> liste = new ArrayList<>();
        while (rs.next()) {
            liste.add(new Compagnie(rs.getInt("idcompagnie"), rs.getString("nom")));
        }

        return liste;
    }
    
}
