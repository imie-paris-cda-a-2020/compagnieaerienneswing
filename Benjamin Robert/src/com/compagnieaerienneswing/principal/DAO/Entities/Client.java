/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal.DAO.Entities;

import com.compagnieaerienneswing.principal.DAO.Connexion;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author FiercePC
 */
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer personneIdpersonne;

    private String email;

    private String password;

    public Client() {
    }

    public Client(Integer personneIdpersonne) {
        this.personneIdpersonne = personneIdpersonne;
    }

    public Client(Integer personneIdpersonne, String email, String password) {
        this.personneIdpersonne = personneIdpersonne;
        this.email = email;
        this.password = password;
    }

    public Integer getPersonneIdpersonne() {
        return personneIdpersonne;
    }

    public void setPersonneIdpersonne(Integer personneIdpersonne) {
        this.personneIdpersonne = personneIdpersonne;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personneIdpersonne != null ? personneIdpersonne.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.personneIdpersonne == null && other.personneIdpersonne != null) || (this.personneIdpersonne != null && !this.personneIdpersonne.equals(other.personneIdpersonne))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.compagnieaerienneswing.principal.DAO.Entities.Client[ personneIdpersonne=" + personneIdpersonne + " ]";
    }

    public static void saveClient(Client cl) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Insert Into client (personne_idpersonne,email,password) values(?,?,?)  ");
        ps.setInt(1, cl.getPersonneIdpersonne());
        ps.setString(2, cl.getEmail());
        ps.setString(3, cl.getPassword());
        ps.execute();
        ps.close();
        System.err.println("Insert OK");
    }

    public static void updateClient(Client cl) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Update client set email = ?,password = ? where personne_idpersonne = ?  ");
        ps.setString(1, cl.getEmail());
        ps.setString(2, cl.getPassword());
        ps.setInt(3, cl.getPersonneIdpersonne());
        ps.executeUpdate();
        ps.close();
        System.err.println("Update OK");
    }

    public static void deleteClient(Client cl) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("delete from client where personne_idpersonne = ? ");
        ps.setInt(1, cl.getPersonneIdpersonne());
        ps.execute();
        ps.close();
        System.err.println("Delete OK");
    }

    public static ArrayList<Client> getAllClient() throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Select * from client order by personne_idpersonne");
        ResultSet rs = ps.executeQuery();
        ArrayList<Client> liste = new ArrayList<>();
        while (rs.next()) {

            Client cl = new Client();
            cl.setPersonneIdpersonne(rs.getInt("personne_idpersonne"));
            cl.setEmail(rs.getString("email"));
            cl.setPassword("password");
            liste.add(cl);
        }

        return liste;
    }

}
