/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal.DAO.Entities;

import com.compagnieaerienneswing.principal.DAO.Connexion;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author FiercePC
 */
public class Aeroport implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idaeroport;

    private String nom;

    private String ville;

    private int pays;

    public Aeroport() {
    }

    public Aeroport(String idaeroport) {
        this.idaeroport = idaeroport;
    }

    public Aeroport(String idaeroport, String nom, String ville, int pays) {
        this.idaeroport = idaeroport;
        this.nom = nom;
        this.ville = ville;
        this.pays = pays;
    }

    public String getIdaeroport() {
        return idaeroport;
    }

    public void setIdaeroport(String idaeroport) {
        this.idaeroport = idaeroport;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getPays() {
        return pays;
    }

    public void setPays(int pays) {
        this.pays = pays;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idaeroport != null ? idaeroport.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aeroport)) {
            return false;
        }
        Aeroport other = (Aeroport) object;
        if ((this.idaeroport == null && other.idaeroport != null) || (this.idaeroport != null && !this.idaeroport.equals(other.idaeroport))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.compagnieaerienneswing.principal.DAO.Entities.Aeroport[ idaeroport=" + idaeroport + " ]";
    }

    public static void saveAeroport(Aeroport a) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Insert into aeroport (idaeroport,nom,ville,pays) values(?,?,?,?)  ");
        ps.setString(1, a.getIdaeroport());
        ps.setString(2, a.getNom());
        ps.setString(3, a.getVille());
        ps.setInt(4, a.getPays());
          
        ps.execute();
        ps.close();
        System.err.println("Insert OK");
    }

    public static void updateAeroport(Aeroport a) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Update aeroport set nom = ?,ville = ?,pays = ? where idaeroport = ?  ");
        ps.setString(1, a.getNom());
        ps.setString(2, a.getVille());
        ps.setInt(3, a.getPays());
        ps.setString(4, a.getIdaeroport());
        ps.executeUpdate();
        System.err.println("allo ?");
        ps.close();
        System.err.println("Update OK");
    }

    public static void deleteAeroport(Aeroport a) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("delete from aeroport where idaeroport = ? ");
        ps.setString(1, a.getIdaeroport());
        ps.execute();
        ps.close();
        System.err.println("Delete OK");
    }

    public static ArrayList<Aeroport> getAllAeroport() throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Select * from aeroport order by idaeroport");
        ResultSet rs = ps.executeQuery();
        ArrayList<Aeroport> liste = new ArrayList<>();
        while (rs.next()) {

            Aeroport at = new Aeroport();
            at.setIdaeroport(rs.getString("idaeroport"));
            at.setNom(rs.getString("nom"));
            at.setVille(rs.getString("ville"));
            at.setPays(Integer.parseInt(rs.getString("pays")));
            liste.add(at);
        }

        return liste;
    }

}
