/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal.DAO.Entities;

import com.compagnieaerienneswing.principal.DAO.Connexion;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author FiercePC
 */
public class Personne implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idpersonne;

    private String nom;

    private String prenom;

    private Date dateNaiss;

    private String adresse;

    private String ville;

    private String zipcode;

    private Integer paysIdpays;

    public Personne(Integer idpersonne) {
        this.idpersonne = idpersonne;
    }

    public Personne(Integer idpersonne, String nom, String prenom, Date dateNaiss, String adresse, String ville, String zipcode, int paysIdpays) {
        this.idpersonne = idpersonne;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaiss = dateNaiss;
        this.adresse = adresse;
        this.ville = ville;
        this.zipcode = zipcode;
        this.paysIdpays = paysIdpays;
    }

    public Personne(String nom, String prenom, Date dateNaiss, String adresse, String ville, String zipcode, int paysIdpays) {

        this.nom = nom;
        this.prenom = prenom;
        this.dateNaiss = dateNaiss;
        this.adresse = adresse;
        this.ville = ville;
        this.zipcode = zipcode;
        this.paysIdpays = paysIdpays;
    }

    public Integer getIdpersonne() {
        return idpersonne;
    }

    public void setIdpersonne(Integer idpersonne) {
        this.idpersonne = idpersonne;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaiss() {
        return dateNaiss;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Integer getPaysIdpays() {
        return paysIdpays;
    }

    public void setPaysIdpays(Integer paysIdpays) {
        this.paysIdpays = paysIdpays;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpersonne != null ? idpersonne.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personne)) {
            return false;
        }
        Personne other = (Personne) object;
        if ((this.idpersonne == null && other.idpersonne != null) || (this.idpersonne != null && !this.idpersonne.equals(other.idpersonne))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.compagnieaerienneswing.principal.DAO.Entities.Personne[ idpersonne=" + idpersonne + " ]";
    }

    public static void savePersonne(Personne p) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Insert Into Personne(nom,prenom,dateNaiss,adresse,ville,zipcode,pays_idpays) values(?,?,?,?,?,?,?)  ");
        ps.setString(1, p.getNom());
        ps.setString(2, p.getPrenom());
        ps.setDate(3, p.getDateNaiss());
        ps.setString(4, p.getAdresse());
        ps.setString(5, p.getVille());
        ps.setString(6, p.getZipcode());
        ps.setInt(7, p.getPaysIdpays());
        ps.execute();
        ps.close();
        System.err.println("Insert OK");
    }

    public static void updatePersonne(Personne p) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Update Personne set nom = ?,prenom= ?,dateNaiss= ?,adresse= ?,ville= ?,zipcode= ?,pays_idpays= ? where idpersonne = ?  ");

        ps.setString(1, p.getNom());
        ps.setString(2, p.getPrenom());
        ps.setDate(3, p.getDateNaiss());
        ps.setString(4, p.getAdresse());
        ps.setString(5, p.getVille());
        ps.setString(6, p.getZipcode());
        ps.setInt(7, p.getPaysIdpays());
        ps.setInt(8, p.getIdpersonne());
        ps.executeUpdate();
        ps.close();
        System.err.println("Update OK");
    }

    public static void deletePersonne(Personne p) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("delete from personne where idpersonne = ? ");
        ps.setInt(1, p.getIdpersonne());
        ps.execute();
        ps.close();
        System.err.println("Delete OK");
    }

    public static ArrayList<Personne> geAllPersonne() throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Select * from personne order by idpersonne");
        ResultSet rs = ps.executeQuery();
        ArrayList<Personne>liste = new ArrayList<>();
        while(rs.next()){
            liste.add(new Personne(rs.getInt("idpersonne"),rs.getString("nom"),rs.getString("prenom"),rs.getDate("dateNaiss"),rs.getString("adresse"),rs.getString("ville"),rs.getString("zipcode"),rs.getInt("pays_idpays")));
        }
        
        return liste;
    }
}
