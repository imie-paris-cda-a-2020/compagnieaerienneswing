/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal.DAO.Entities;

import com.compagnieaerienneswing.principal.DAO.Connexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author FiercePC
 */
public class Pays {

    private int id;

    private String nom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Pays(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Pays(int id) {
        this.id = id;
    }

    public Pays(String nom) {
        this.nom = nom;
    }
    
      public static void savePays(Pays p) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Insert Into pays (nom) values(?)  ");
        ps.setString(1, p.getNom());
        ps.execute();
        ps.close();
        System.err.println("Insert OK");
    }

    public static void updatePays(Pays p) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Update pays set nom = ? where id = ?  ");
        ps.setString(1, p.getNom());
        ps.setInt(2, p.getId());
        ps.executeUpdate();
        ps.close();
        System.err.println("Update OK");
    }

    public static void deletePays(Pays p) throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("delete from pays where id = ? ");
        ps.setInt(1, p.getId());
        ps.execute();
        ps.close();
        System.err.println("Delete OK");
    }

    public static ArrayList<Pays> getAllPays() throws SQLException {
        Connection c = Connexion.getInstance().getCo();
        PreparedStatement ps = c.prepareStatement("Select * from pays order by id");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pays> liste = new ArrayList<>();
        while (rs.next()) {
            liste.add(new Pays(rs.getInt("id"), rs.getString("nom")));
        }

        return liste;
    }
}
