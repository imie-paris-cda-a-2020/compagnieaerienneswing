/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal.DAO;

import java.sql.DriverManager;
import java.sql.DriverManager;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FiercePC
 */
public class Connexion {

    private static Connexion INSTANCE;
    private static Connection co;
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/airbabouchedb?userSSL=false&zeroDateTimeBehavior=convertToNull";
    static final String USER = "root";
    static final String PASS = "";

    private Connexion() {
        try {
            co = DriverManager.getConnection(DB_URL, USER, PASS);
            System.err.println(co);
            System.err.println("connexion created is " + co.isClosed());

        } catch (SQLException ex) {
            Logger.getLogger(Connexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Connexion getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Connexion();
        }

        try {
            if (co.isClosed()) {
                co = DriverManager.getConnection(DB_URL, USER, PASS);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return INSTANCE;
    }

    public Connection getCo() {
        return co;
    }

}
