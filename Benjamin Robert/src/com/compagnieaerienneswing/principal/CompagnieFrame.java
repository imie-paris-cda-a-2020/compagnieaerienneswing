/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal;

import com.compagnieaerienneswing.principal.DAO.Entities.Compagnie;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author FiercePC
 */
public class CompagnieFrame extends JFrame{
    public static CompagnieFrame openCompagnieFrame() {
        CompagnieFrame  vf = new CompagnieFrame();
        vf.setSize(700, 500);
        vf.setTitle("CRUD sur la table Compagnie");
        vf.setResizable(false);
        GroupLayout layout = new GroupLayout(vf.getContentPane());
        vf.getContentPane().setLayout(layout);

        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new String[]{"id", "nom"});
        model.addRow(new String[]{"id", "nom"});
        try {
            for (Compagnie p : Compagnie.getAllCompagnie()) {
                model.addRow(new String[]{Integer.toString(p.getIdcompagnie()), p.getNom()});
            }
        } catch (SQLException ex) {
   
        }

        JTable liste = new JTable(model);
        JButton get = new JButton("Refresh liste Compagnie");
        get.addActionListener((e) -> {

            refreshTable(liste);
        });

        JButton insert = new JButton("Add Compagnie");
        JTextField id = new JTextField("id");
        JTextField name = new JTextField("name");

        insert.addActionListener((e) -> {
            try {

                Compagnie p = new Compagnie();
                p.setNom(name.getText());
                Compagnie.saveCompagnie(p);
                refreshTable(liste);
            } catch (SQLException ex) {
              
            }
        });

        JButton update = new JButton("update Pays");
        update.addActionListener((e) -> {

            try {
             
              Compagnie comp = new Compagnie( Integer.parseInt(id.getText()),name.getText());
                Compagnie.updateCompagnie(comp);
                refreshTable(liste);
            } catch (SQLException ex) {
                Logger.getLogger(CompagnieFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

 
        JButton delete = new JButton("delete Pays");
        delete.addActionListener((e) -> {
            int i = Integer.parseInt(id.getText());
            try {

                Compagnie.deleteCompagnie(new Compagnie(i));
                refreshTable(liste);
            } catch (SQLException ex) {

            }
        });

        layout.setHorizontalGroup(layout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(insert, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addComponent(id, 60, 60, 60)
                        .addComponent(name, 60, 60, 60)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(update, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(delete, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(get, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addComponent(liste, 500, 500, 500)
                        .addContainerGap()
                )
        );
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(insert, 60, 60, 60)
                                .addComponent(id, 40, 40, 40)
                                .addComponent(name, 40, 40, 40)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(update, 60, 60, 60)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(delete, 60, 60, 60)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(get, 60, 60, 60)
                                .addComponent(liste, 140, 140, 140)
                        )
                        .addContainerGap(188, Short.MAX_VALUE))
        );

        return vf;
    }

    private static void refreshTable(JTable liste) {
        DefaultTableModel md = new DefaultTableModel();
        md.setColumnIdentifiers(new String[]{"idcompagnie", "nom"});
        md.addRow(new String[]{"idcompagnie", "nom"});

        try {
            for (Compagnie p : Compagnie.getAllCompagnie()) {
                md.addRow(new String[]{Integer.toString(p.getIdcompagnie()), p.getNom()});
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        liste.setModel(md);
    }
}
