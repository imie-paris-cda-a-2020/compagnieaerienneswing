/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal;

import com.compagnieaerienneswing.principal.DAO.Connexion;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author FiercePC
 */
public class CompagnieAerienneSwing extends JFrame {

    private static void onExit(Connexion c) {
        try {
            c.getCo().close();
        } catch (SQLException ex) {
            Logger.getLogger(CompagnieAerienneSwing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException {
        Connexion co = Connexion.getInstance();
        try {
            CompagnieAerienneSwing cas = new CompagnieAerienneSwing();

            cas.setDefaultCloseOperation(EXIT_ON_CLOSE);
            cas.setTitle("CRUD Swing Eval");
            cas.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent evt) {
                    onExit(co);
                }
            });
            JButton PersonneOverlay = new JButton("ouvrir personne Frame");
            PersonneOverlay.addActionListener((e) -> {
                PersonneFrame pf = PersonneFrame.openPersonneFrame();
                pf.setVisible(true);
            });

            JButton PaysOverlay = new JButton("ouvrir pays frame");
            PaysOverlay.addActionListener((e) -> {
                PaysFrame pf = PaysFrame.openPersonneFrame();
                pf.setVisible(true);
            });

            JButton CompagnieOverlay = new JButton("ouvrir compagnie frame");
            CompagnieOverlay.addActionListener((e) -> {
                CompagnieFrame cf = CompagnieFrame.openCompagnieFrame();
                cf.setVisible(true);
            });

            JButton AeroportOverlay = new JButton("ouvrir aeroport frame");
            AeroportOverlay.addActionListener((e) -> {
                AeroportFrame cf = AeroportFrame.openAeroportFrame();
                cf.setVisible(true);
            });
             JButton ClientOverlay = new JButton("ouvrir client frame");
            ClientOverlay.addActionListener((e) -> {
                ClientFrame cf = ClientFrame.openClientFrame();
                cf.setVisible(true);
            });

            GroupLayout layout = new GroupLayout(cas.getContentPane());
            cas.getContentPane().setLayout(layout);
            layout.setHorizontalGroup(layout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(PersonneOverlay, 200, 200, 200)
                    )
                    .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(PaysOverlay, 200, 200, 200)
                    )
                    .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(CompagnieOverlay, 200, 200, 200)
                    )
                    .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(AeroportOverlay, 200, 200, 200)
                    )
                     .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(ClientOverlay, 200, 200, 200)
                    )
            );
            layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(PersonneOverlay, 60, 60, 60)
                            )
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(PaysOverlay, 60, 60, 60)
                            )
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(CompagnieOverlay, 60, 60, 60)
                            )
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(AeroportOverlay, 60, 60, 60)
                            )
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(ClientOverlay, 60, 60, 60)
                            )
                            .addContainerGap(188, Short.MAX_VALUE))
            );
            cas.setSize(1000, 750);
            cas.add(PersonneOverlay);
            cas.setVisible(true);
        } catch (Exception e) {
            System.err.println(e.toString());
        } finally {
            // co.getCo().close();
        }
    }

}
