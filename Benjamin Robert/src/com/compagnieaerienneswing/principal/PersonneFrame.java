/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal;

import com.compagnieaerienneswing.principal.DAO.Entities.Personne;
import java.sql.Date;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author FiercePC
 */
public class PersonneFrame extends JFrame {

    public static PersonneFrame openPersonneFrame() {
        PersonneFrame vf = new PersonneFrame();
        vf.setSize(700, 500);
        vf.setTitle("CRUD sur la table Personne");
        vf.setResizable(false);
        GroupLayout layout = new GroupLayout(vf.getContentPane());
        vf.getContentPane().setLayout(layout);

        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new String[]{"idpersonne", "nom", "prenom", "dateNaiss", "adresse", "ville", "zipcode", "pays_idpays"});
        model.addRow(new String[]{"idpersonne", "nom", "prenom", "dateNaiss", "adresse", "ville", "zipcode", "pays_idpays"});
        try {
            for (Personne p : Personne.geAllPersonne()) {
                model.addRow(new String[]{Integer.toString(p.getIdpersonne()), p.getNom(), p.getPrenom(), p.getDateNaiss().toString(), p.getAdresse(), p.getVille(), p.getZipcode(), Integer.toString(p.getPaysIdpays())});
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        JTable liste = new JTable(model);
        JButton get = new JButton("Refresh liste personne");
        get.addActionListener((e) -> {

            refreshTable(liste);
        });

        JButton insert = new JButton("Add Personne");
        JTextField id = new JTextField("id");
        JTextField name = new JTextField("name");
        JTextField lastname = new JTextField("lastname");
        JTextField datenaiss = new JTextField("dateNaiss");
        JTextField adress = new JTextField("adresse");
        JTextField ville = new JTextField("ville");
        JTextField zipcode = new JTextField("zipcode");
        JTextField pays = new JTextField("pays");
        insert.addActionListener((e) -> {
            try {
                java.util.Date d = new SimpleDateFormat("yyyy-MM-dd").parse(datenaiss.getText());
                System.err.println(d);
                Personne p = new Personne(name.getText(), lastname.getText(), new java.sql.Date(d.getTime()), adress.getText(), ville.getText(), zipcode.getText(), Integer.parseInt(pays.getText()));
                Personne.savePersonne(p);
                refreshTable(liste);
            } catch (SQLException ex) {
                Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        JButton update = new JButton("update Personne");
        update.addActionListener((e) -> {

            try {
                java.util.Date d = new SimpleDateFormat("yyyy-MM-dd").parse(datenaiss.getText());
                System.err.println(d);
                Personne p = new Personne(
                        Integer.parseInt(id.getText()),
                        name.getText(),
                        lastname.getText(),
                        new java.sql.Date(d.getTime()),
                        adress.getText(),
                        ville.getText(),
                        zipcode.getText(),
                        Integer.parseInt(pays.getText()));
                Personne.updatePersonne(p);
                refreshTable(liste);
            } catch (SQLException ex) {
                Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        JButton delete = new JButton("delete Personne");
        delete.addActionListener((e) -> {
            int i = Integer.parseInt(id.getText());
            try {
                Personne.deletePersonne(new Personne(i));
                refreshTable(liste);
            } catch (SQLException ex) {
                Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        layout.setHorizontalGroup(layout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(insert, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addComponent(id, 60, 60, 60)
                        .addComponent(name, 60, 60, 60)
                        .addComponent(lastname, 60, 60, 60)
                        .addComponent(datenaiss, 60, 60, 60)
                        .addComponent(adress, 60, 60, 60)
                        .addComponent(ville, 60, 60, 60)
                        .addComponent(zipcode, 60, 60, 60)
                        .addComponent(pays, 60, 60, 60)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(update, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(delete, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(get, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addComponent(liste, 500, 500, 500)
                        .addContainerGap()
                )
        );
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(insert, 60, 60, 60)
                                .addComponent(id, 40, 40, 40)
                                .addComponent(name, 40, 40, 40)
                                .addComponent(lastname, 40, 40, 40)
                                .addComponent(datenaiss, 40, 40, 40)
                                .addComponent(adress, 40, 40, 40)
                                .addComponent(ville, 40, 40, 40)
                                .addComponent(zipcode, 40, 40, 40)
                                .addComponent(pays, 40, 40, 40)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(update, 60, 60, 60)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(delete, 60, 60, 60)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(get, 60, 60, 60)
                                .addComponent(liste, 140, 140, 140)
                        )
                        .addContainerGap(188, Short.MAX_VALUE))
        );

        return vf;
    }

    private static void refreshTable(JTable liste) {
        DefaultTableModel md = new DefaultTableModel();
        md.setColumnIdentifiers(new String[]{"idpersonne", "nom", "prenom", "dateNaiss", "adresse", "ville", "zipcode", "pays_idpays"});
        md.addRow(new String[]{"idpersonne", "nom", "prenom", "dateNaiss", "adresse", "ville", "zipcode", "pays_idpays"});

        try {
            for (Personne p : Personne.geAllPersonne()) {
                md.addRow(new String[]{Integer.toString(p.getIdpersonne()), p.getNom(), p.getPrenom(), p.getDateNaiss().toString(), p.getAdresse(), p.getVille(), p.getZipcode(), Integer.toString(p.getPaysIdpays())});
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        liste.setModel(md);
    }
}
