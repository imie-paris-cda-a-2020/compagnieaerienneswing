/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal;

import com.compagnieaerienneswing.principal.DAO.Entities.Client;
import com.compagnieaerienneswing.principal.DAO.Entities.Pays;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author FiercePC
 */
public class ClientFrame extends JFrame {

    public static ClientFrame openClientFrame() {
        ClientFrame vf = new ClientFrame();
        vf.setSize(700, 500);
        vf.setTitle("CRUD sur la table Client");
        vf.setResizable(false);
        GroupLayout layout = new GroupLayout(vf.getContentPane());
        vf.getContentPane().setLayout(layout);

        DefaultTableModel model = new DefaultTableModel();

        JTable liste = new JTable(model);
        ClientFrame.refreshTable(liste);
        JButton get = new JButton("Refresh liste Client");
        get.addActionListener((e) -> {

            refreshTable(liste);
        });

        JButton insert = new JButton("Add Client");
        JTextField id = new JTextField("id personne");
        JTextField email = new JTextField("email");
        JTextField password = new JTextField("password");
        insert.addActionListener((e) -> {
            try {

                Client cl = new Client();
                cl.setPersonneIdpersonne(Integer.parseInt(id.getText()));
                cl.setEmail(email.getText());
                cl.setPassword(password.getText());
                Client.saveClient(cl);
                refreshTable(liste);
            } catch (SQLException ex) {
                Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        JButton update = new JButton("update Client");
        update.addActionListener((e) -> {

            try {

                Client cl = new Client();
                cl.setPersonneIdpersonne(Integer.parseInt(id.getText()));
                cl.setPassword(password.getText());
                cl.setEmail(email.getText());
                Client.updateClient(cl);
                refreshTable(liste);
            } catch (SQLException ex) {
                Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });


        JButton delete = new JButton("delete Client");
        delete.addActionListener((e) -> {
            try {
                Client.deleteClient(new Client(Integer.parseInt(id.getText())));
                refreshTable(liste);
            } catch (SQLException ex) {
                Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        layout.setHorizontalGroup(layout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(insert, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addComponent(id, 60, 60, 60)
                        .addComponent(email, 60, 60, 60)
                        .addComponent(password, 60, 60, 60)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(update, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(delete, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(get, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addComponent(liste, 500, 500, 500)
                        .addContainerGap()
                )
        );
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(insert, 60, 60, 60)
                                .addComponent(id, 40, 40, 40)
                                .addComponent(email, 40, 40, 40)
                                .addComponent(password, 40, 40, 40)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(update, 60, 60, 60)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(delete, 60, 60, 60)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(get, 60, 60, 60)
                                .addComponent(liste, 140, 140, 140)
                        )
                        .addContainerGap(188, Short.MAX_VALUE))
        );

        return vf;
    }

    private static void refreshTable(JTable liste) {
        DefaultTableModel md = new DefaultTableModel();
        md.setColumnIdentifiers(new String[]{"personne_idpersonne", "email", "password"});
        md.addRow(new String[]{"personne_idpersonne", "email", "password"});

        try {
            for (Client c : Client.getAllClient()) {
                md.addRow(new String[]{Integer.toString(c.getPersonneIdpersonne()), c.getEmail(), c.getPassword()});
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonneFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        liste.setModel(md);
    }
}
