/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal;

import com.compagnieaerienneswing.principal.DAO.Entities.Aeroport;
import com.compagnieaerienneswing.principal.DAO.Entities.Compagnie;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author FiercePC
 */
public class AeroportFrame extends JFrame{

    public static AeroportFrame openAeroportFrame() {
        AeroportFrame vf = new AeroportFrame();
        vf.setSize(700, 500);
        vf.setTitle("CRUD sur la table Aeroport");
        vf.setResizable(false);
        GroupLayout layout = new GroupLayout(vf.getContentPane());
        vf.getContentPane().setLayout(layout);
        DefaultTableModel model = new DefaultTableModel();        
        JTable liste = new JTable(model);
        AeroportFrame.refreshTable(liste);
        JButton get = new JButton("Refresh liste Aeroport");
        get.addActionListener((e) -> {
            refreshTable(liste);
        });

        JButton insert = new JButton("Add Aeroport");
        JTextField id = new JTextField("idaeroport");
        JTextField name = new JTextField("nom");
        JTextField ville = new JTextField("ville");
        JTextField pays = new JTextField("pays");

        insert.addActionListener((e) -> {
            try {
                
                Aeroport a = new Aeroport();
                a.setIdaeroport(id.getText());
                a.setNom(name.getText());
                a.setVille(ville.getText());
                a.setPays(Integer.parseInt(pays.getText()));
                Aeroport.saveAeroport(a);

                refreshTable(liste);
            } catch (SQLException ex) {
                 Logger.getLogger(Aeroport.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        JButton update = new JButton("update Pays");
        update.addActionListener((e) -> {
            try {
                Aeroport a = new Aeroport();
                a.setIdaeroport(id.getText());
                a.setNom(name.getText());
                a.setVille(ville.getText());
                a.setPays(Integer.parseInt(pays.getText()));
                Aeroport.updateAeroport(a);
                refreshTable(liste);
            } catch (SQLException ex) {
                Logger.getLogger(CompagnieFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        JButton delete = new JButton("delete Pays");
        delete.addActionListener((e) -> {
          
            try {

                Aeroport.deleteAeroport(new Aeroport(id.getText()));
                refreshTable(liste);
            } catch (SQLException ex) {

            }
        });

        layout.setHorizontalGroup(layout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(insert, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addComponent(id, 60, 60, 60)
                        .addComponent(name, 60, 60, 60)
                        .addComponent(ville, 60, 60, 60)
                        .addComponent(pays, 60, 60, 60)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(update, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(delete, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addContainerGap()
                )
                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(get, 120, 120, 120)
                        .addGap(5, 5, 5)
                        .addComponent(liste, 500, 500, 500)
                        .addContainerGap()
                )
        );
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(insert, 60, 60, 60)
                                .addComponent(id, 40, 40, 40)
                                .addComponent(name, 40, 40, 40)
                                .addComponent(ville, 40, 40, 40)
                                .addComponent(pays, 40, 40, 40)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(update, 60, 60, 60)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(delete, 60, 60, 60)
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(get, 60, 60, 60)
                                .addComponent(liste, 140, 140, 140)
                        )
                        .addContainerGap(188, Short.MAX_VALUE))
        );

        return vf;
    }

    private static void refreshTable(JTable liste) {
        DefaultTableModel md = new DefaultTableModel();
        md.setColumnIdentifiers(new String[]{"idaeroport", "nom", "ville", "pays"});
        md.addRow(new String[]{"idaeroport", "nom", "ville", "pays"});

        try {
            for (Aeroport p : Aeroport.getAllAeroport()) {
                md.addRow(new String[]{p.getIdaeroport(), p.getNom(), p.getVille(), Integer.toString(p.getPays())});
            }
        } catch (SQLException ex) {

        }
        liste.setModel(md);
    }
}
