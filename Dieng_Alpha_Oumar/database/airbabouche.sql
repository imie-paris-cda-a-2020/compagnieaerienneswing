-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : Dim 10 jan. 2021 à 19:00
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `airbabouche`
--

-- --------------------------------------------------------

--
-- Structure de la table `aeroport`
--

DROP TABLE IF EXISTS `aeroport`;
CREATE TABLE IF NOT EXISTS `aeroport` (
  `idaeroport` varchar(5) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `ville` varchar(70) NOT NULL,
  `pays` int(11) NOT NULL,
  PRIMARY KEY (`idaeroport`),
  KEY `FK_aeroport_pays` (`pays`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `aeroport`
--

INSERT INTO `aeroport` (`idaeroport`, `nom`, `ville`, `pays`) VALUES
('1', 'Aéroport de Paris-Charles de Gaulle', 'Roissy-en-France', 1),
('2', 'Aéroport de Paris-Orly', 'Orly', 2),
('3', 'Aéroport de Paris-Le Bourget ', 'Le Bourget', 1),
('4', 'Aéroport de Paris-Orly', 'orly', 1);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `email` varchar(200) NOT NULL,
  `password` varchar(80) NOT NULL,
  `personne_idpersonne` int(11) NOT NULL,
  PRIMARY KEY (`personne_idpersonne`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`email`, `password`, `personne_idpersonne`) VALUES
('aod@gmail.com', 'aod', 1),
('jkbrow@outlook.com', 'jkb', 2),
('Oumar@yahoo.fr', 'ouma', 3),
('taleb@gmail.com', 'GH', 4);

-- --------------------------------------------------------

--
-- Structure de la table `compagnie`
--

DROP TABLE IF EXISTS `compagnie`;
CREATE TABLE IF NOT EXISTS `compagnie` (
  `idcompagnie` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`idcompagnie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `compagnie`
--

INSERT INTO `compagnie` (`idcompagnie`, `nom`) VALUES
(1, 'Air France'),
(2, 'Air Caraibes'),
(3, 'Air China'),
(4, 'American Airlines');

-- --------------------------------------------------------

--
-- Structure de la table `escale`
--

DROP TABLE IF EXISTS `escale`;
CREATE TABLE IF NOT EXISTS `escale` (
  `vol_idvol` int(11) NOT NULL,
  `aeroport_idaeroport` varchar(5) NOT NULL,
  `date_depart` varchar(30) NOT NULL,
  `date_arrive` varchar(30) NOT NULL,
  PRIMARY KEY (`vol_idvol`,`aeroport_idaeroport`),
  KEY `fk_escale_aeroport1_idx` (`aeroport_idaeroport`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `escale`
--

INSERT INTO `escale` (`vol_idvol`, `aeroport_idaeroport`, `date_depart`, `date_arrive`) VALUES
(1, '1', '2021-01-10 09:58:08', '2021-01-11 00:58:08'),
(1, '2', '2021-12-12 20:58:08', '2021-12-12 20:58:08'),
(1, '3', '14/01/2021 00:23:00', '13/01/2021 19:23:00'),
(2, '2', '15/01/2021 23:00:00', '16/01/2021 10:23:00'),
(2, '3', '15/01/2021 13:10:00', '16/01/2021 00:23:00'),
(2, '4', '18/01/2021 10:10:00', '18/01/2021 20:23:00');

-- --------------------------------------------------------

--
-- Structure de la table `passager`
--

DROP TABLE IF EXISTS `passager`;
CREATE TABLE IF NOT EXISTS `passager` (
  `idpassager` int(11) NOT NULL AUTO_INCREMENT,
  `idpersonne` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpassager`),
  KEY `FK_passager_personne` (`idpersonne`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `passager`
--

INSERT INTO `passager` (`idpassager`, `idpersonne`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

DROP TABLE IF EXISTS `pays`;
CREATE TABLE IF NOT EXISTS `pays` (
  `idPays` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`idPays`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `pays`
--

INSERT INTO `pays` (`idPays`, `nom`) VALUES
(1, 'France'),
(2, 'etats unis'),
(3, 'Guinée');

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `idpersonne` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(70) NOT NULL,
  `prenom` varchar(70) NOT NULL,
  `dateNaiss` varchar(30) NOT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `pays_idPays` int(11) NOT NULL,
  PRIMARY KEY (`idpersonne`),
  KEY `fk_personne_pays1_idx` (`pays_idPays`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `personne`
--

INSERT INTO `personne` (`idpersonne`, `nom`, `prenom`, `dateNaiss`, `adresse`, `ville`, `zipcode`, `pays_idPays`) VALUES
(1, 'Dieng', 'Alpha', '1997-01-30', '11 place du Général de Gaulle', 'Ivry sur seine', '123R34', 1),
(2, 'Brown', 'Jack', '2000-10-12', '5 Bryant Park', 'New York', '1234P', 2),
(3, 'Diallo', 'Oumar', '2001-11-21', '66 place de concorde ', 'paris', '123G45', 1),
(4, 'Taleb', 'Claire', '1984-05-01', '11 place du général de gaulle ', 'ivry sur seine', '123T', 1);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `idvol` int(11) NOT NULL,
  `client_idpersonne` int(11) NOT NULL,
  `confirmation` enum('Confirm','Pending','Cancel','Refuse') NOT NULL,
  `personne_idpersonne` int(11) NOT NULL,
  PRIMARY KEY (`idvol`,`client_idpersonne`),
  KEY `FK_reservation_vol` (`idvol`),
  KEY `fk_reservation_client1_idx` (`client_idpersonne`),
  KEY `fk_reservation_personne1_idx` (`personne_idpersonne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`idvol`, `client_idpersonne`, `confirmation`, `personne_idpersonne`) VALUES
(1, 1, 'Confirm', 1),
(2, 1, 'Cancel', 2),
(2, 2, 'Pending', 2);

-- --------------------------------------------------------

--
-- Structure de la table `vol`
--

DROP TABLE IF EXISTS `vol`;
CREATE TABLE IF NOT EXISTS `vol` (
  `idvol` int(11) NOT NULL AUTO_INCREMENT,
  `place` smallint(6) NOT NULL DEFAULT '300',
  `intitule` varchar(50) DEFAULT NULL,
  `aeroport_depart` varchar(5) NOT NULL,
  `aeroport_arrive` varchar(5) NOT NULL,
  `date_depart` varchar(30) NOT NULL,
  `date_arrive` varchar(30) NOT NULL,
  `idcompagnie` int(11) NOT NULL,
  PRIMARY KEY (`idvol`),
  KEY `FK_vol_aeroport` (`aeroport_depart`),
  KEY `FK_vol_aeroport_2` (`aeroport_arrive`),
  KEY `FK_vol_compagnie` (`idcompagnie`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vol`
--

INSERT INTO `vol` (`idvol`, `place`, `intitule`, `aeroport_depart`, `aeroport_arrive`, `date_depart`, `date_arrive`, `idcompagnie`) VALUES
(1, 300, 'Vol 1', '1', '2', '2021-01-10 09:55:48', '2021-01-11 00:55:48', 4),
(2, 300, 'Vol 2', '2', '1', '2021-01-12 19:55:48', '2021-01-13 07:55:48', 1),
(3, 222, 'VOL 3', '1', '2', '12/01/2021', '13/01/2021', 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `aeroport`
--
ALTER TABLE `aeroport`
  ADD CONSTRAINT `FK_aeroport_pays` FOREIGN KEY (`pays`) REFERENCES `pays` (`idPays`);

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `fk_client_personne1` FOREIGN KEY (`personne_idpersonne`) REFERENCES `personne` (`idpersonne`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `escale`
--
ALTER TABLE `escale`
  ADD CONSTRAINT `fk_escale_aeroport1` FOREIGN KEY (`aeroport_idaeroport`) REFERENCES `aeroport` (`idaeroport`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_escale_vol1` FOREIGN KEY (`vol_idvol`) REFERENCES `vol` (`idvol`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `passager`
--
ALTER TABLE `passager`
  ADD CONSTRAINT `FK_passager_personne` FOREIGN KEY (`idpersonne`) REFERENCES `personne` (`idpersonne`);

--
-- Contraintes pour la table `personne`
--
ALTER TABLE `personne`
  ADD CONSTRAINT `fk_personne_pays1` FOREIGN KEY (`pays_idPays`) REFERENCES `pays` (`idPays`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK_reservation_vol` FOREIGN KEY (`idvol`) REFERENCES `vol` (`idvol`),
  ADD CONSTRAINT `fk_reservation_client1` FOREIGN KEY (`client_idpersonne`) REFERENCES `client` (`personne_idpersonne`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reservation_personne1` FOREIGN KEY (`personne_idpersonne`) REFERENCES `personne` (`idpersonne`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `vol`
--
ALTER TABLE `vol`
  ADD CONSTRAINT `FK_vol_aeroport` FOREIGN KEY (`aeroport_depart`) REFERENCES `aeroport` (`idaeroport`),
  ADD CONSTRAINT `FK_vol_aeroport_2` FOREIGN KEY (`aeroport_arrive`) REFERENCES `aeroport` (`idaeroport`),
  ADD CONSTRAINT `FK_vol_compagnie` FOREIGN KEY (`idcompagnie`) REFERENCES `compagnie` (`idcompagnie`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
